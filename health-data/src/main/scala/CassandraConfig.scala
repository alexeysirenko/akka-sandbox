
case class CassandraConfig(host: String, port: Int, keyspace: String, table: String)