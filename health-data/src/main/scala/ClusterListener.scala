import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem}
import akka.cluster.ClusterEvent._
import akka.cluster.{Cluster, MemberStatus}

class ClusterListener(readable: ActorRef, writable: ActorRef) extends Actor with ActorLogging {

  Cluster(context.system).subscribe(self, classOf[ClusterDomainEvent])

  override def receive: Receive = {
    case MemberUp(member) =>
      log.info(s"Member $member is up!")
      if (member.hasRole("writable")) {
        // TODO: tell this writer to update a readable?
      }
      if (member.hasRole("readable")) {
        // TODO: tell all writers to update a readable?
      }

    case MemberExited(member) =>
      log.info(s"Member $member is exited")
    case MemberRemoved(member, previousState) =>
      if(previousState == MemberStatus.Exiting) {
        log.info(s"Member $member Previously gracefully exited, REMOVED.")
      } else {
        log.info(s"$member Previously downed after unreachable, REMOVED.")
      }
    case UnreachableMember(member) =>
      log.info(s"$member UNREACHABLE")
    case ReachableMember(member) =>
      log.info(s"$member REACHABLE")
    case state: CurrentClusterState =>
      log.info(s"Current state of the cluster: $state")
  }

  override def postStop(): Unit = {
    Cluster(context.system).unsubscribe(self)
    super.postStop()
  }

}
