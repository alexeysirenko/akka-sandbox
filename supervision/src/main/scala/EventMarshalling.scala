
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

case class Error(message: String)

trait EventMarshalling extends SprayJsonSupport with DefaultJsonProtocol {

  import Worker._

  implicit val requestFormat = jsonFormat1(Request)
  implicit val responseFormat = jsonFormat1(Response)
  implicit val errorFormat = jsonFormat1(Error)
}
