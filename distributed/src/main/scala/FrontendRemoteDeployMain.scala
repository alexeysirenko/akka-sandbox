import FrontendMain.system
import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import com.typesafe.config.ConfigFactory

object FrontendRemoteDeployMain extends App with Startup {
  val config = ConfigFactory.load("frontend-remote-deploy")
  implicit val system = ActorSystem("frontend", config)

  val api = new RestApi() {
    override val log = Logging(system.eventStream, "frontend-remote-deploy")
    override implicit val requestTimeout = configuredRequestTimeout(config)
    override implicit def executionContext = system.dispatcher

    override def createBoxOffice: ActorRef = {
      system.actorOf(
        BoxOffice.props,
        BoxOffice.name
      )
    }
  }

  startup(api.routes)
}
