import akka.actor.{ActorSystem, Props}
import akka.actor.FSM.{CurrentState, SubscribeTransitionCallBack, Transition}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}

import concurrent.duration._

class BookStoreTest extends TestKit(ActorSystem("BookStoreTest"))
  with WordSpecLike with BeforeAndAfterAll with MustMatchers
  with ImplicitSender {

  override def afterAll(): Unit = {
    system.terminate()
  }

  "BookStoreTest" must {
    "follow the flow" in {
      val bookStore = system.actorOf(Props(new BookStore()))
      val bookStateListener = TestProbe()
      bookStore ! SubscribeTransitionCallBack(bookStateListener.ref)

      bookStateListener.expectMsg(CurrentState(bookStore, Closed))

      bookStore ! AddBookRequest(Book("foo"))
      expectNoMessage(3 seconds)
      bookStateListener.expectNoMessage(3 seconds)
      bookStore ! GetBooksRequest
      expectNoMessage(3 seconds)
      bookStateListener.expectNoMessage(3 seconds)

      bookStore ! OpenRequest
      bookStateListener.expectMsg(Transition(bookStore, Closed, Open))

      bookStore ! AddBookRequest(Book("bar"))
      bookStore ! GetBooksRequest
      expectMsg(BooksResponse(List(Book("bar"))))
      bookStateListener.expectNoMessage(3 seconds)
    }
  }

}
