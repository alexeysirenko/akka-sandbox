import akka.actor.SupervisorStrategy._

import scala.concurrent.duration._
import akka.actor.{Actor, ActorLogging, AllForOneStrategy, OneForOneStrategy, Props, SupervisorStrategy}
import akka.routing.RoundRobinPool

class Supervisor extends Actor with ActorLogging {

  override def preStart() = log.info("Starting a supervisor...")
  override def postStop() = log.info("Supervisor stopped")


  def one: SupervisorStrategy = {
    OneForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 10 seconds) {
      case _: ArithmeticException => Resume
      case _: NullPointerException => Restart
      case _: IllegalArgumentException => Stop
      case _: Exception => Escalate
    }
  }

  def all: SupervisorStrategy = {
    AllForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 10 seconds) {
      case _: ArithmeticException => Resume
      case _: NullPointerException => Restart
      case _: IllegalArgumentException => Stop
      case _: Exception => Escalate
    }
  }

  def oneCustom = OneForOneStrategy() {
    case _ => Restart
  }

  val workersRouter =  context.actorOf(RoundRobinPool(5, supervisorStrategy = oneCustom).props(Props[Worker]), "worker-router")
  //val worker = context.actorOf(Props(new Worker), "worker")

  override def receive: Receive = {
    case message => workersRouter forward message
  }
}
