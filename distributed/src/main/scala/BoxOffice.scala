import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.event.LoggingReceive
import akka.util.Timeout

import scala.concurrent.Future


object BoxOffice {

  def props(implicit timeout: Timeout) = Props(new BoxOffice)
  def name = "boxOffice"

  case class CreateEvent(name: String, tickets: Int)
  case class GetEvent(name: String)
  case object GetEvents


  case class GetTickets(name: String, tickets: Int)
  case class CancelEvent(name: String)

  case class Event(name: String, tickets: Int)
  case class Events(events: List[Event])

  sealed trait EventResponse
  case class EventCreated(event: Event) extends EventResponse
  case object EventExists extends EventResponse

}

class BoxOffice(implicit timeout: Timeout) extends Actor with ActorLogging {

  import BoxOffice._
  import context._

  def createTicketSeller(eventName: String) = context.actorOf(TicketSeller.props(eventName), eventName)

  override def receive: Receive = LoggingReceive {
    case CreateEvent(name, tickets) =>
      def create() = {
        val ticketSeller = createTicketSeller(name)
        val newTickets = (1 to tickets).map { ticketId =>
          TicketSeller.Ticket(ticketId)
        }.toList
        ticketSeller ! TicketSeller.Add(newTickets)
        sender() ! EventCreated(Event(name, tickets))
      }
      context.child(name).fold(create())(_ => sender() ! EventExists)

    case GetEvent(name) =>
      val existingTicketSeller = context.child(name)
      existingTicketSeller match {
        case Some(eventActor) => eventActor forward TicketSeller.GetEvent
        case None => sender() ! None
      }

    case GetTickets(name, tickets) =>
      def notFound() = sender() ! TicketSeller.Tickets(name)
      def buy(existingTicketSeller: ActorRef) = existingTicketSeller forward TicketSeller.Buy(tickets)

      context.child(name).fold(notFound())(buy)

    case GetEvents =>
      import akka.pattern.ask
      import akka.pattern.pipe

      def getEvents = context.children.map { seller =>
        self.ask(GetEvent(seller.path.name)).mapTo[Option[Event]]
      }
      def convertToEvents(f: Future[Iterable[Option[Event]]]) =
        f.map(_.flatten).map(l=> Events(l.toList))

      pipe(convertToEvents(Future.sequence(getEvents))) to sender()

    case CancelEvent(name) =>
      def notFound() = sender() ! None
      def cancelEvent(child: ActorRef) = child forward TicketSeller.Cancel
      context.child(name).fold(notFound())(cancelEvent)
  }



}
