import akka.actor.Actor

class EchoActor extends Actor {
  override def receive: Receive = {
    case message: Any => sender() ! message
  }
}
