import java.nio.file.{FileSystems, Files, Path}
import java.nio.file.StandardOpenOption.{APPEND, CREATE, WRITE}

import Readable.HealthDataWrittenSinkRequest
import akka.NotUsed
import akka.actor.SupervisorStrategy.Stop
import akka.pattern.pipe
import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill, Props, ReceiveTimeout}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.LoggingReceive
import akka.routing.ConsistentHashingRouter.ConsistentHashable
import akka.stream._
import akka.stream.scaladsl.{Broadcast, FileIO, Flow, GraphDSL, Keep, Sink, Source, StreamRefs}
import akka.util.{ByteString, Timeout}
import spray.json._
import akka.pattern.ask
import akka.stream.alpakka.cassandra.scaladsl.CassandraFlow
import com.datastax.driver.core.PreparedStatement
import com.datastax.driver.core.{Session => CassandraSession}
import com.datastax.driver.core.{Cluster => CassandraCluster}

import scala.concurrent.duration._
import scala.concurrent.Future

object Writable {

  def props(cassandraConfig: CassandraConfig, readable: ActorRef, timeout: Timeout): Props =
    Props(new Writable(cassandraConfig, readable, timeout))

  case class HealthDataWriteSinkRequest(userId: String) extends ConsistentHashable {
    override def consistentHashKey: Any = userId.hashCode
  }

  case class HealthDataWriteSinkReady(userId: String, sinkRef: SinkRef[WriteHealthData]) extends ConsistentHashable {
    override def consistentHashKey: Any = userId.hashCode
  }

  case class WriteHealthData(userId: String, healthData: HealthData)

  case class HealthDataWritten(userId: String, healthData: HealthData)

  // case class CreateKeySpaceRequest(keySpace: String)

}

class Writable(cassandraConfig: CassandraConfig, readable: ActorRef, timeout: Timeout) extends Actor with ActorLogging with DataMarshalling {

  import Writable._

  import context.dispatcher
  implicit val mat = ActorMaterializer()(context)
  implicit val requestTimeout = timeout

  implicit lazy val cassandra = CassandraCluster.builder
    .addContactPoint(cassandraConfig.host)
    .withPort(cassandraConfig.port)
    .build
    .connect()

  lazy val writeHealthDataToCassandra = Flow[WriteHealthData].map { writeHealthData =>
    writeHealthData.healthData
  }

  lazy val preparedStatement =
    cassandra.prepare(s"INSERT INTO ${cassandraConfig.keyspace}.${cassandraConfig.table}(id, userId, heartbeat) VALUES (uuid(), ?, ?)")

  lazy val statementBinder = (healthData: HealthData, statement: PreparedStatement) =>
    statement.bind(healthData.userId.orNull, new java.lang.Integer(healthData.heartbeat))

  def cassandraWriteFlow(userId: String) = CassandraFlow
    .createWithPassThrough[HealthData](parallelism = 2, preparedStatement, statementBinder)
    .map(healthData => HealthDataWritten(userId, healthData))

  override def preStart() = {
    // self ! CreateKeySpaceRequest("healthdata")
  }

  override def receive: Receive = LoggingReceive {
    case HealthDataWriteSinkRequest(userId: String) =>
      log.info(s"Received a sink request for user $userId. Responding...")

      val saveAndForwardSinkRef = readable.ask(Readable.HealthDataWrittenSinkRequest(userId))
        .mapTo[Readable.HealthDataWrittenSinkReady].flatMap { sinkReady =>

          val writeFlow = Flow[WriteHealthData].map { writeHealthData =>
            writeHealthData.healthData
          }.via(cassandraWriteFlow(userId))

          val forwardWrittenSink = Flow[Writable.HealthDataWritten].to(sinkReady.sinkRef.sink())

          val mergedSink = Sink.fromGraph(GraphDSL.create() { implicit builder =>
            import GraphDSL.Implicits._
            val bcast = builder.add(Broadcast[WriteHealthData](1))
            bcast ~> writeFlow ~> forwardWrittenSink
            SinkShape(bcast.in)
          })
          StreamRefs.sinkRef[WriteHealthData]().to(mergedSink).run()
        }

      val reply: Future[HealthDataWriteSinkReady] = saveAndForwardSinkRef.map(HealthDataWriteSinkReady(userId, _))

      // reply to sender
      reply.pipeTo(sender())
  }


}
