import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}

import scala.io.StdIn

object Main extends App with RequestTimeout with EventMarshalling {

  val config = ConfigFactory.load()
  val host = config.getString("http.host") // Gets the host and a port from the configuration
  val port = config.getInt("http.port")

  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer() // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher
  implicit val timeout = requestTimeout(config)

  val supervisor = system.actorOf(Props(new Supervisor), "supervisor")

  import StatusCodes._

  val routes = path("hello") {
    post {
      entity(as[Worker.Request]) { request =>
        onSuccess(supervisor.ask(request)) {
          case response: Worker.Response => complete(OK, response)
        }
      }
    }
  }

  val bindingFuture = Http().bindAndHandle(routes, "0.0.0.0", 8080)
  system.log.info(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done
}

trait RequestTimeout {
  import scala.concurrent.duration._
  def requestTimeout(config: Config): Timeout = {
    val t = config.getString("akka.http.server.request-timeout")
    val d = Duration(t)
    FiniteDuration(d.length, d.unit)
  }
}
