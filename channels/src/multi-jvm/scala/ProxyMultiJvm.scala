import akka.actor.{Actor, Props}
import akka.remote.testkit.{MultiNodeConfig, MultiNodeSpec, MultiNodeSpecCallbacks}
import akka.testkit.ImplicitSender
import akka.testkit.TestActors.EchoActor
import org.scalatest.{BeforeAndAfterAll, FunSuiteLike, MustMatchers, WordSpecLike}

trait STMultiNodeSpec extends MultiNodeSpecCallbacks with WordSpecLike with BeforeAndAfterAll with MustMatchers {

  override def beforeAll(): Unit = multiNodeSpecBeforeAll()

  override def afterAll(): Unit = multiNodeSpecAfterAll()

}

object ReliableProxySampleConfig extends MultiNodeConfig {
  val client = role("Client")
  val server = role("Server")
  testTransport(on = true)
}

class ReliableProxySampleSpecMultiJvmNode1 extends ReliableProxySample
class ReliableProxySampleSpecMultiJvmNode2 extends ReliableProxySample

import akka.remote.transport.ThrottlerTransportAdapter.Direction
import scala.concurrent.duration._
import concurrent.Await
import akka.contrib.pattern.ReliableProxy



class ReliableProxySample extends MultiNodeSpec(ReliableProxySampleConfig) with STMultiNodeSpec with ImplicitSender {
  import ReliableProxySampleConfig._

  override def initialParticipants: Int = roles.size

  "A MultiNodeSample" must {

    "wait or all nodes to enter a barrier" in {
      enterBarrier("startup")
    }

    "send to and receive from a remote node" in {
      runOn(client) {
        println("entering deployed...")
        enterBarrier("deployed")
        val pathToEcho = node(server) / "user" / "echo"
        val echo = system.actorSelection(pathToEcho)
        val proxy = system.actorOf(ReliableProxy.props(pathToEcho, 500.millis), "proxy")

        println("echo...")
        proxy ! "message1"
        expectMsg("message1")

        println("await...")
        Await.ready(
          testConductor.blackhole(client, server, Direction.Both), 1 second
        )

        println("direct...")
        echo ! "DirectMessage"
        proxy ! "ProxyMessage"
        expectNoMessage(3 seconds)

        println("await...")
        Await.ready(
          testConductor.passThrough(client, server, Direction.Both), 1 second
        )

        expectMsg("ProxyMessage")

        println("direct...")
        echo ! "DirectMessage2"
        expectMsg("DirectMessage2")
      }

      runOn(server) {
        system.actorOf(Props(new Actor {
          def receive = {
            case msg: AnyRef => {
              sender() ! msg
            }
          }
        }), "echo")
        enterBarrier("deployed")
      }

      enterBarrier("finished")
    }
  }
}
