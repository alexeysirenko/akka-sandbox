import akka.actor.ActorSystem
import akka.cluster.Cluster
import akka.stream.ActorMaterializer
import com.typesafe.config.{Config, ConfigFactory}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.util.Timeout


object Main extends App {
  val config = ConfigFactory.load()
  val system = ActorSystem("health-data", config)

  println(s"Starting node with roles: ${Cluster(system).selfRoles}")


}
