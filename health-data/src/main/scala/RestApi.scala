import java.nio.file.Path
import java.nio.file.StandardOpenOption.{APPEND, CREATE, WRITE}

import Writable.{HealthDataWritten, WriteHealthData}
import akka.{Done, NotUsed}
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.cluster.routing.{ClusterRouterPool, ClusterRouterPoolSettings}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.http.scaladsl.common.EntityStreamingSupport
import akka.pattern.ask
import akka.routing.{BroadcastPool, ConsistentHashingPool}
import akka.stream.{ActorMaterializer, IOResult, SinkShape}
import akka.stream.scaladsl.{Broadcast, FileIO, Flow, GraphDSL, Keep, Sink, StreamRefs}
import akka.util.{ByteString, Timeout}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class RestApi(system: ActorSystem, readable: ActorRef, writable: ActorRef, timeout: Timeout)
             (implicit val materializer: ActorMaterializer)
  extends DataMarshalling {

  //import StatusCodes._

  implicit val requestTimeout = timeout
  implicit val executionContext = system.dispatcher
  implicit val jsonStreamingSupport = EntityStreamingSupport.json()

  def route: Route = healthDataRoute

  def healthDataRoute = pathPrefix("health" / Segment) { userId =>
    pathEndOrSingleSlash {
      post {
        entity(asSourceOf[HealthData]) { src =>
          onComplete({
            ask(writable, Writable.HealthDataWriteSinkRequest(userId)).mapTo[Writable.HealthDataWriteSinkReady].map { sinkReady =>
              src
                .map(hd => WriteHealthData(userId, hd.copy(userId = Some(userId))))
                .toMat(sinkReady.sinkRef.sink())(Keep.right).run()
            }
          }) {
            case Success(_) =>
              complete(StatusCodes.OK)
            case Failure(e) =>
              complete((
                StatusCodes.BadRequest,
                ParseError(userId, e.getMessage)
              ))
          }
        }
      } ~
      get {
        onSuccess(ask(readable, Readable.ReadHealthDataRequest(userId)).mapTo[Readable.ReadHealthDataResponse]) { hda =>
          complete(StatusCodes.OK, HealthDataResponse(userId, hda.healthData))
        }
      }
    }
  }


}


