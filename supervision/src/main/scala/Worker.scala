import akka.actor.{Actor, ActorLogging}

object Worker {
  case class Request(text: String)
  case class Response(text: String)
}

class Worker extends Actor with ActorLogging {
  import Worker._

  override def preRestart(reason: Throwable, message: Option[Any]) = {
    log.info(s"Restarting worker... ${self.path.name}")
    super.preRestart(reason, message)
  }

  override def postRestart(reason: Throwable) = {
    log.info(s"...worker ${self.path.name}: restart completed!")
    super.postRestart(reason)
  }

  override def preStart() = log.info(s"Starting worker ${self.path.name}...")
  override def postStop() = log.info(s"worker ${self.path.name} stopped")

  override def receive: Receive = {
    case Request(text: String) =>
      log.info(s"Worker ${self.path.name} received request `$text`")
      text match {
        case "arithmetic" => throw new ArithmeticException
        case "null" => throw new NullPointerException
        case "illegal" => throw new IllegalArgumentException
        case "exception" => throw new Exception
        case "throwable" => throw new Throwable("error")
        case _ => sender() ! Response(text)
      }
    case _ => log.info(s"Unknown request message")
  }
}
