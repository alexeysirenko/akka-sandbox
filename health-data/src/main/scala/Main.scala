import java.nio.file.{FileSystems, Files}

import Writable.HealthDataWriteSinkRequest
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.cluster.Cluster
import akka.cluster.routing.{ClusterRouterPool, ClusterRouterPoolSettings}
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings, ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.stream.ActorMaterializer
import com.typesafe.config.{Config, ConfigFactory}
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.routing.ConsistentHashingPool
import akka.util.Timeout
import com.datastax.driver.core.{Cluster => CassandraCluster}

import scala.collection.script.End
import scala.concurrent.Future
import scala.io.StdIn


object Main extends App with RequestTimeout with ReadableNode {
  val config = ConfigFactory.load()

  val cassandraConfig = CassandraConfig(
    config.getString("cassandra.host"),
    config.getInt("cassandra.port"),
    config.getString("cassandra.keyspace"),
    config.getString("cassandra.table"))

  override implicit val system = ActorSystem("health-data", config)
  implicit val materializer = ActorMaterializer() // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher

  println(s"Starting node with roles: ${Cluster(system).selfRoles}")

  val readable = createReadable(cassandraConfig)

  if (system.settings.config.getStringList("akka.cluster.roles").contains("master")) {
    Cluster(system).registerOnMemberUp {
      val host = config.getString("http.host")
      val port = config.getInt("http.port")

      val readableProxy = createReadableProxy
      val writableRouter = system.actorOf(Writable.props(cassandraConfig, readableProxy, requestTimeout(config)))

      val api = new RestApi(system, readableProxy, writableRouter, requestTimeout(config)).route

      system.actorOf(Props(new ClusterListener(readableProxy, writableRouter)), "cluster-listener")

      val bindingFuture = Http().bindAndHandle(api, host, port)
      println(s"Server online at http://$host:$port/\nPress RETURN to stop...")
      StdIn.readLine() // let it run until user presses return
      bindingFuture
        .flatMap(_.unbind()) // trigger unbinding from the port
        .onComplete(_ => system.terminate()) // and shutdown when done
    }
  }

}

trait RequestTimeout {
  import scala.concurrent.duration._
  def requestTimeout(config: Config): Timeout = {
    val t = config.getString("akka.http.server.request-timeout")
    val d = Duration(t)
    FiniteDuration(d.length, d.unit)
  }
}

trait ReadableNode {

  def system: ActorSystem

  def createReadable(cassandraConfig: CassandraConfig) = system.actorOf(
    ClusterSingletonManager.props(
      singletonProps = Readable.props(cassandraConfig),
      terminationMessage = End,
      settings = ClusterSingletonManagerSettings(system).withRole("readable")),
    name = Readable.actorName)

  def createReadableProxy = system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = s"/user/${Readable.actorName}",
      settings = ClusterSingletonProxySettings(system).withRole("readable")),
    name = s"${Readable.actorName}-proxy")
}




