name := "all-sandbox"

version := "1.0"

lazy val sandbox = project.in(file("sandbox"))

lazy val supervision = project.in(file("supervision"))

lazy val distributed = project.in(file("distributed"))

lazy val channels = project.in(file("channels"))

lazy val state = project.in(file("state"))

lazy val streaming = project.in(file("streaming"))

lazy val cluster = project.in(file("cluster"))

lazy val `health-data` = project.in(file("health-data"))

lazy val `stream-refs` = project.in(file("stream-refs"))
