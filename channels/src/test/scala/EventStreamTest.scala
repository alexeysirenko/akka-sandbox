import akka.testkit.{TestKit, TestProbe}
import akka.actor.{ActorSystem, DeadLetter, PoisonPill, Props}
import org.scalatest._
import java.util.Date

import scala.concurrent.duration._

case class TestMessage1(text: String)
case class TestMessage2(text: String)

class EventStreamTest extends TestKit(ActorSystem("EventStreamTest"))
  with FunSuiteLike with BeforeAndAfterAll with MustMatchers {

  override def afterAll(): Unit = system.terminate()

  test("EventStream should distribute messages") {
    val receiver1 = TestProbe()
    val receiver2 = TestProbe()

    system.eventStream.subscribe(
      receiver1.ref,
      classOf[TestMessage1]
    )

    system.eventStream.subscribe(
      receiver2.ref,
      classOf[TestMessage2]
    )

    val message1 = TestMessage1("foo")
    val message2 = TestMessage2("foo")

    system.eventStream.publish(message1)
    system.eventStream.publish(message2)

    receiver1.expectMsg(message1)
    receiver2.expectMsg(message2)
  }


  test("catch messages send to deadLetters") {
    val deadLetterMonitor = TestProbe()
    system.eventStream.subscribe(
      deadLetterMonitor.ref,
      classOf[DeadLetter]
    )

    val msg1 = TestMessage1("foo")
    val msg2 = TestMessage2("bar")

    system.deadLetters ! msg1

    val dead1 = deadLetterMonitor.expectMsgType[DeadLetter]
    dead1.message must be(msg1)

    val echoActor = system.actorOf(Props(new EchoActor))

    echoActor ! PoisonPill
    echoActor ! msg2


    val dead2 = deadLetterMonitor.expectMsgType[DeadLetter]
    dead2.message must be(msg2)
  }

  test("") {

  }





}