import spray.json._

case class HealthData(userId: Option[String] = None, heartbeat: Int)
case class HealthDataResponse(userId: String, healthData: Option[HealthData])

case class Error(message: String)
case class ParseError(userId: String, msg: String)

trait DataMarshalling extends DefaultJsonProtocol {
  //import BoxOffice._

  implicit val healthDataFormat = jsonFormat2(HealthData)
  implicit val errorFormat = jsonFormat1(Error)
  implicit val hdResponseFormat = jsonFormat2(HealthDataResponse)
  implicit val parseErrorFormat = jsonFormat2(ParseError)
}
