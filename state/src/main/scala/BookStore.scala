import akka.actor.{Actor, FSM}

sealed trait State
case object Open extends State
case object Closed extends State

case object OpenRequest
case object CloseRequest
case class Book(name: String)
case class AddBookRequest(book: Book)
case object GetBooksRequest
case class BooksResponse(books: List[Book])

case class StateData(books: List[Book])

class BookStore extends Actor with FSM[State, StateData] {

  startWith(Closed, StateData(List.empty[Book]))

  when(Closed) {
    case Event(OpenRequest, data: StateData) =>
      goto(Open)
    case Event(AddBookRequest(book), data) =>
      stay
  }
  when(Open) {
    case Event(CloseRequest, data: StateData) =>
      goto(Closed)
    case Event(AddBookRequest(book), data) =>
      stay using StateData(data.books :+ book)
    case Event(GetBooksRequest, state) =>
      sender() ! BooksResponse(state.books)
      stay()
  }
  whenUnhandled {
    case Event(e, s) => {
      log.warning("received unhandled request {} in state {}/{}", e, stateName, s)
      stay
    }
  }
  initialize()

  onTransition {
    case before -> after => log.warning("Changing state from {} to {}", before, after)
  }

}
