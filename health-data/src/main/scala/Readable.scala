import java.nio.file.StandardOpenOption.{APPEND, CREATE, WRITE}
import java.nio.file.{FileSystems, Files}

import Writable.HealthDataWritten
import akka.actor.SupervisorStrategy.Stop
import akka.actor.{Actor, ActorLogging, Props, ReceiveTimeout}
import akka.event.LoggingReceive
import akka.pattern.pipe
import akka.stream.alpakka.cassandra.scaladsl.CassandraSource
import akka.stream.scaladsl.{FileIO, Flow, Sink, Source, StreamRefs}
import akka.stream.{ActorMaterializer, IOResult, SinkRef}
import akka.util.ByteString

import scala.concurrent.Future
import scala.concurrent.duration._
import com.datastax.driver.core.{SimpleStatement, Cluster => CassandraCluster}

object Readable {

  val actorName = "readable"

  def props(cassandraConfig: CassandraConfig): Props = Props(new Readable(cassandraConfig))

  case class HealthDataWrittenSinkRequest(userId: String)

  case class HealthDataWrittenSinkReady(userId: String, sinkRef: SinkRef[HealthDataWritten])

  case class ReadHealthDataRequest(userId: String)

  case class ReadHealthDataResponse(userId: String, healthData: Option[HealthData])

  case class HealthDataAggregated(total: Int, count: Int) {
    def append(data: HealthData): HealthDataAggregated = this.copy(total = this.total + data.heartbeat, count = this.count + 1)
  }

  object HealthDataAggregated {
    val empty = HealthDataAggregated(0, 0)
  }

}

class Readable(cassandraConfig: CassandraConfig) extends Actor with ActorLogging {

  import Readable._
  import Writable._

  implicit lazy val cassandra = CassandraCluster.builder
    .addContactPoint(cassandraConfig.host)
    .withPort(cassandraConfig.port)
    .build
    .connect()

  import context.dispatcher
  implicit val mat = ActorMaterializer()(context)

  var aggregatedHealthData: Map[String, HealthDataAggregated] = Map.empty // TODO: become/unbecome

  val updateHealthDataSink = Sink.foreach[HealthData] { healthData =>
    healthData.userId.foreach { userId =>
      val newAggregatedData = aggregatedHealthData.getOrElse(userId, HealthDataAggregated.empty).append(healthData)
      aggregatedHealthData += (userId -> newAggregatedData)
    }
  }

  val healthDataWrittenSink = Flow[HealthDataWritten]
    .map { case HealthDataWritten(userId, healthData) => healthData}.to(updateHealthDataSink)

  val stmt = new SimpleStatement(s"SELECT userId, heartbeat FROM ${cassandraConfig.keyspace}.${cassandraConfig.table}")

  readState() // Read pre-existing state from the database

  override def receive: Receive = LoggingReceive {
    case HealthDataWrittenSinkRequest(userId: String) =>
      log.error(s"Received a sink request for user $userId. Responding...")
      // obtain the source you want to offer:

      // materialize the SinkRef (the remote is like a source of data for us):
      val ref: Future[SinkRef[HealthDataWritten]] = StreamRefs.sinkRef[HealthDataWritten]().to(healthDataWrittenSink).run()

      // wrap the SinkRef in some domain message, such that the sender knows what source it is
      val reply: Future[HealthDataWrittenSinkReady] = ref.map(HealthDataWrittenSinkReady(userId, _))

      // reply to sender
      reply.pipeTo(sender())

    case ReadHealthDataRequest(userId) =>
      val hd = aggregatedHealthData.get(userId).map { hda =>
        HealthData(Some(userId), hda.total / hda.count)
      }
      sender ! ReadHealthDataResponse(userId, hd)

  }

  def readState() = CassandraSource(stmt).map(row =>
    HealthData(Option(row.getString("userId")), row.getInt("heartbeat"))
  ).runWith(updateHealthDataSink)



}
