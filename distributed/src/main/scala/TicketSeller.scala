import akka.actor.{Actor, PoisonPill, Props}

object TicketSeller {
  def props(event: String) = Props(new TicketSeller(event))

  case class Ticket(id: Int)
  case class Add(tickets: List[Ticket])
  case class Buy(tickets: Int)
  case class Tickets(event: String, tickets: List[Ticket] = List.empty)

  case object GetEvent
  case object Cancel
}

class TicketSeller(event: String) extends Actor {

  import TicketSeller._

  var tickets: List[Ticket] = List.empty

  override def receive: Receive = {
    case Add(newTickets) => tickets = tickets ++ newTickets
    case Buy(nTickets) =>
      val entries = tickets.take(nTickets)
      if (entries.size >= nTickets) {
        sender() ! Tickets(event, entries)
        tickets = tickets.drop(nTickets)
      } else sender() ! Tickets(event)
    case GetEvent => sender() ! Some(BoxOffice.Event(event, tickets.size))
    case Cancel =>
      sender() ! Some(BoxOffice.Event(event, tickets.size))
      self ! PoisonPill
  }
}
