import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory

object BackendMain extends App with RequestTimeout {
  val config = ConfigFactory.load("backend")
  val system = ActorSystem("backend", config)
  implicit val timeout = configuredRequestTimeout(config)
  system.actorOf(Props(new BoxOffice), BoxOffice.name)
}
