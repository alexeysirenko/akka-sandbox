
name := "health-data"

version := "0.1"

scalaVersion := "2.12.5"

libraryDependencies ++= {
  val akkaVersion = "2.5.11"
  val akkaHttpVersion = "10.1.0"
  Seq(
    "com.typesafe.akka" %% "akka-actor"      % akkaVersion,
    "com.typesafe.akka" %% "akka-http-core"  % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-http-spray-json"  % akkaHttpVersion,
    //"com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j"      % akkaVersion,
    "ch.qos.logback"    %  "logback-classic" % "1.2.3",
    "com.typesafe.akka" %% "akka-testkit"    % akkaVersion   % "test",
    "org.scalatest"     %% "scalatest"       % "3.0.5"       % "test",
    "com.typesafe.akka" %% "akka-remote" % akkaVersion,
    "com.typesafe.akka" %% "akka-multi-node-testkit" % akkaVersion % "test",
    "com.github.romix.akka" %% "akka-kryo-serialization" % "0.5.1",
    "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-metrics" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
    "com.lightbend.akka" %% "akka-stream-alpakka-cassandra" % "0.19"
  )
}

// Assembly settings
mainClass in assembly := Some("Main")

assemblyJarName in assembly := "health-data.jar"
        